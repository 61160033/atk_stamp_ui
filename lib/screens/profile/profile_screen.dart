import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';

class ProfileScr extends StatefulWidget {
  ProfileScr({Key key}) : super(key: key);

  @override
  State<ProfileScr> createState() => _ProfileScrState();
}

class _ProfileScrState extends State<ProfileScr> {
  String image;
  bool check = true;

  TextEditingController email = TextEditingController();
  TextEditingController username = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    await setState(() {
      FirebaseFirestore.instance
          .collection('user')
          .doc(userEmail?.email)
          .get()
          .then((value) {
        username.text = value.get('name');
        email.text = userEmail?.email;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Profile",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? ClipOval(
                                  child: Image.network(
                                    image,
                                    width: 150,
                                    height: 150,
                                    fit: BoxFit.cover,
                                  ),
                                )
                              : ClipOval(
                                  child: Image.network(
                                    userEmail?.photoUrl,
                                    width: 150,
                                    height: 150,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text(
                                'ยังไม่ยืนยันรูป',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  fontFamily: 'Kanit',
                                ),
                              ),
                              alignment: Alignment.center,
                              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              width: 130,
                              decoration: BoxDecoration(
                                color: LightColors.kRedSmooth,
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushNamed(context, '/profileQR');
                              },
                              child: Container(
                                child: Text(
                                  'แสดง QR code',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 18,
                                    fontFamily: 'Kanit',
                                  ),
                                ),
                                alignment: Alignment.center,
                                width: 200,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: Colors.lightBlueAccent,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  MyTextField(
                    label: 'ชื่อ',
                    enable: false,
                    controller: username,
                  ),
                  MyTextField(
                    label: 'e-mail',
                    controller: email,
                    enable: false,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
