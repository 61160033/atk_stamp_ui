import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class ProfileVr extends StatefulWidget {
  ProfileVr({Key key}) : super(key: key);

  @override
  State<ProfileVr> createState() => _ProfileVrState();
}

class _ProfileVrState extends State<ProfileVr> {
  String image;
  bool check = true;

  CollectionReference user = FirebaseFirestore.instance.collection('user');

  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController username = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    setState(() {
      user
          .doc(userEmail.email?.toString())
          .collection('atk_result')
          .orderBy('date_time', descending: false)
          .get()
          .then((QuerySnapshot querySnapshot) {
        check = querySnapshot.docs.last.get('result');
        image = querySnapshot.docs.last.get('image');
        DocumentReference<Map<String, dynamic>> atk =
            querySnapshot.docs.last.get('atk_brand');
        print(querySnapshot.docs.last.id);
        atk.get().then((value) {
          atkBrand.text = value['atk_brand'];
        });
        DocumentReference<Map<String, dynamic>> name =
            querySnapshot.docs.last.get('name');
        name.get().then((value) {
          username.text = value['name'];
        });
        var format = new DateFormat('d-MM-y, hh:mm:ss a');
        dateTime.text =
            format.format(querySnapshot.docs.last.get('date_time').toDate());
        GeoPoint geo = querySnapshot.docs.last.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;

  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Profile",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Image.network(
                              'https://docs.flutter.dev/assets/images/dash/dash-fainting.gif'),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 80,
                    width: width,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/');
                          },
                          child: Container(
                            child: Text(
                              'ไม่อนุมัติ',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                                fontFamily: 'Kanit',
                              ),
                            ),
                            alignment: Alignment.center,
                            margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                            width: 130,
                            decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/homepage');
                          },
                          child: Container(
                            child: Text(
                              'อนุมัติ',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 18,
                                fontFamily: 'Kanit',
                              ),
                            ),
                            alignment: Alignment.center,
                            margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                            width: 130,
                            decoration: BoxDecoration(
                              color: Colors.blue,
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }
}
