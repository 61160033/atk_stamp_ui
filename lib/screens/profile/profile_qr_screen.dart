import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CreateQRProfile extends StatefulWidget {
  CreateQRProfile({Key key}) : super(key: key);
  @override
  _CreateQRProfileState createState() => _CreateQRProfileState();
}

class _CreateQRProfileState extends State<CreateQRProfile> {
  String userRef;
  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    await FirebaseFirestore.instance
        .collection('user')
        .doc(userEmail?.email)
        .get()
        .then((value) {
      setState(() {
        userRef = value.reference.path;
      });
      print(value.reference.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      body: ListView(
        children: [
          Center(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 50),
                  const Text(
                    'Create CODE',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontSize: 35,
                      fontFamily: 'Kanit',
                    ),
                  ),
                  const SizedBox(height: 20),
                  userRef != null
                      ? QrImage(
                          data: userRef,
                          size: 350,
                          backgroundColor: Colors.white,
                        )
                      : Container(),
                  const SizedBox(height: 80),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              height: 50,
              child: Text(
                'เสร็จสิ้น',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                  fontFamily: 'Kanit',
                ),
              ),
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: LightColors.kBlue,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          )
        ],
      ),
    );
  }
}
