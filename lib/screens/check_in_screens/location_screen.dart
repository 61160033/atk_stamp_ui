import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';

class LocationPage extends StatefulWidget {
  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  Stream<QuerySnapshot> listLocate;
  @override
  void initState() {
    super.initState();
    _getLocate();
  }

  _getLocate() async {
    DocumentReference<Map<String, dynamic>> companyRef;
    await FirebaseFirestore.instance
        .collection('user')
        .doc(userEmail?.email)
        .get()
        .then((value) {
      setState(() {
        companyRef = value.get('company');
      });
    });
    setState(() {
      listLocate = FirebaseFirestore.instance
          .doc(companyRef?.path)
          .collection('locations')
          .snapshots();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Location",
          style: TextStyle(
            fontFamily: 'Kanit',
          ),
        ),
      ),
      body: listLocate != null
          ? ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: [
                StreamBuilder(
                  stream: listLocate,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Text('Something went wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Text('Loading..');
                    }
                    return ListBody(
                      children: snapshot.data.docs.map((document) {
                        return FutureBuilder(
                            future: Future.wait([]),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.hasData) {
                                return Container(
                                  decoration: BoxDecoration(
                                    color:
                                        Colors.cyan.shade100.withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  margin: EdgeInsets.all(5),
                                  child: GestureDetector(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        document.get('image') != null
                                            ? Image.network(
                                                document.get('image'),
                                                fit: BoxFit.cover,
                                                width: 100,
                                                height: 75,
                                              )
                                            : Container(),
                                        Expanded(
                                          child: Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 10, vertical: 5),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                    'สถานที่ : ' +
                                                        (document
                                                            .get('location')),
                                                    style: TextStyle(
                                                      fontSize: 17,
                                                      fontFamily: 'Kanit',
                                                    )),
                                                Text(
                                                    'เวลาเปิด-ปิด : ' +
                                                        (document.get(
                                                            'opening_time')),
                                                    style: TextStyle(
                                                      fontSize: 17,
                                                      fontFamily: 'Kanit',
                                                    )),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    onTap: () async {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (Context) => qrCheckinScan(
                                            locateRef: document.reference.path,
                                          ),
                                        ),
                                      );
                                      print(document.id);
                                    },
                                  ),
                                );
                              } else {
                                return Container();
                              }
                            });
                      }).toList(),
                    );
                  },
                ),
              ],
            )
          : Container(),
    );
  }
}
