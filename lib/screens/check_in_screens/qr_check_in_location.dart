import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class qrCheckinScan extends StatefulWidget {
  final String locateRef;
  qrCheckinScan({Key key, @required this.locateRef}) : super(key: key);

  @override
  _qrCheckinScanState createState() => _qrCheckinScanState(this.locateRef);
}

class _qrCheckinScanState extends State<qrCheckinScan> {
  final String locateRef;
  Barcode barcode;
  final qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController controller;
  CollectionReference user = FirebaseFirestore.instance.collection('user');
  _qrCheckinScanState(this.locateRef);
  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller.pauseCamera();
    } else if (Platform.isIOS) {
      controller.resumeCamera();
    }
  }

  Widget scanScreen(BuildContext context) => QRView(
        key: qrKey,
        onQRViewCreated: onQRViewCreated,
        overlay: QrScannerOverlayShape(
          borderColor: Colors.blueAccent,
          borderRadius: 10,
          borderLength: 20,
          borderWidth: 10,
          cutOutSize: MediaQuery.of(context).size.width * 0.8,
        ),
      );

  void onQRViewCreated(QRViewController controller) {
    setState(() => this.controller = controller);
    controller.scannedDataStream.listen((scanData) {
      print(scanData.code);
      controller.stopCamera();
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => CheckInVerified(
            path: scanData.code,
            locateRef: locateRef,
          ),
        ),
      );
    });
  }

  Widget createControllerButtons() => Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white24,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              onPressed: () async {
                await controller?.toggleFlash();
                setState(() {});
              },
              icon: FutureBuilder<bool>(
                future: controller?.getFlashStatus(),
                builder: (contecr, snapshot) {
                  if (snapshot.data != null) {
                    return Icon(
                      snapshot.data ? Icons.flash_on : Icons.flash_off,
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ),
            IconButton(
              onPressed: () async {
                await controller?.flipCamera();
                setState(() {});
              },
              icon: FutureBuilder<bool>(
                future: controller?.getFlashStatus(),
                builder: (contecr, snapshot) {
                  if (snapshot.data != null) {
                    return const Icon(Icons.switch_camera);
                  } else {
                    return Container();
                  }
                },
              ),
            )
          ],
        ),
      );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          scanScreen(context),
          Positioned(
              child: GestureDetector(
                onTap: () {
                  controller.stopCamera();
                  Navigator.pushNamed(context, '/');
                },
                child: Container(
                  height: 50,
                  child: Text(
                    'กลับหน้าหลัก',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontFamily: 'Kanit',
                        fontSize: 20),
                  ),
                  alignment: Alignment.center,
                  margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: LightColors.kBlue,
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              ),
              bottom: 10),
          Positioned(
            child: createControllerButtons(),
            top: 10,
          ),
        ],
      ),
    );
  }
}
