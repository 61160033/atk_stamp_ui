import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/colors/light_colors.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class CheckInVerified extends StatefulWidget {
  final String path;
  final String locateRef;
  CheckInVerified({Key key, this.path, this.locateRef}) : super(key: key);
  @override
  _CheckInVerifiedState createState() =>
      _CheckInVerifiedState(this.path, this.locateRef);
}

class _CheckInVerifiedState extends State<CheckInVerified> {
  final String _path;
  final String locateRef;
  _CheckInVerifiedState(this._path, this.locateRef);

  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController userName = TextEditingController();
  TextEditingController certifierName = TextEditingController();
  TextEditingController certifierDate = TextEditingController();

  String image;
  bool check = true;
  String updateId = '';
  String addCheckIn = '';
  String atkPath = '';
  @override
  void initState() {
    super.initState();
    setState(() {
      updateId = '';
      addCheckIn = '';
      atkPath = '';
    });
    getDate();
  }

  getDate() async {
    await setState(() {
      FirebaseFirestore.instance.doc(_path).get().then((value) {
        image = value.get('image');
        DocumentReference<Map<String, dynamic>> atk = value.get('atk_brand');
        atk.get().then((brand) {
          atkBrand.text = brand.get('atk_brand');
        });
        DocumentReference<Map<String, dynamic>> name = value.get('name');
        name.get().then((name) {
          userName.text = name.get('name');
        });
        DocumentReference<Map<String, dynamic>> certifier =
            value.get('certifier');
        certifier.get().then((value) {
          certifierName.text = value.get('name');
        });
        var format = new DateFormat('d-MM-y, hh:mm:ss a');
        dateTime.text = format.format(value.get('date_time').toDate());
        certifierDate.text =
            format.format(value.get('confirm_dateTime').toDate());
        GeoPoint geo = value.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
        check = value.get('result');
        addCheckIn = value.reference.parent.parent.path;
        atkPath = value.reference.path;
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;
  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  String checkinRef;
  updateResult(bool verifi) async {
    await FirebaseFirestore.instance
        .doc(addCheckIn)
        .collection('check_in')
        .add({
      'atk_result': FirebaseFirestore.instance.doc(atkPath),
      'check_in_result': verifi,
      'date_time': FieldValue.serverTimestamp(),
      'location': FirebaseFirestore.instance.doc(locateRef),
      'certifier': FirebaseFirestore.instance
          .collection('user')
          .doc(userEmail?.email.toString()),
    });
    print('-------------1--------------');
    await FirebaseFirestore.instance
        .doc(addCheckIn)
        .collection('check_in')
        .orderBy('date_time', descending: false)
        .get()
        .then((value) {
      setState(() {
        checkinRef = value.docs.last.reference.path;
      });
      print(checkinRef);
    });
    print('-------------2--------------');
    await FirebaseFirestore.instance.doc(_path).update({
      'checkin_list': FieldValue.arrayUnion([
        FirebaseFirestore.instance.doc(checkinRef),
      ]),
    }).then((value) => print('update sucess'));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    print(_path);
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              width: width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                                width: double.infinity,
                                padding: EdgeInsets.all(10.0),
                                decoration: new BoxDecoration(
                                  color: check
                                      ? LightColors.kGreenSmooth
                                      : LightColors.kRedSmooth,
                                ),
                                child: new Center(
                                  child: new Text(check ? 'ผ่าน' : 'ไม่ผ่าน',
                                      style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                        fontFamily: 'Kanit',
                                      )),
                                )),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: MyTextField(
                              label: 'ชื่อผู้ยืนยัน',
                              enable: false,
                              controller: certifierName,
                            ),
                          ),
                          Expanded(
                            child: MyTextField(
                              label: 'เวลาที่ยืนยัน',
                              enable: false,
                              controller: certifierDate,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? Image.network(image, width: 200)
                              : Container(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  MyTextField(
                    label: 'ชื่อผู้ตรวจ',
                    enable: false,
                    controller: userName,
                  ),
                  MyTextField(
                    label: 'เวลาที่ตรวจ',
                    enable: false,
                    controller: dateTime,
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    enable: false,
                    controller: address,
                  ),
                  MyTextField(
                    label: 'ยี่ห้อ ATK',
                    enable: false,
                    controller: atkBrand,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () async {
                        await updateResult(false);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (Context) => qrCheckinScan(
                              locateRef: locateRef,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        child: Text(
                          'ไม่อนุญาติให้ \n Check-In',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            fontFamily: 'Kanit',
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: GestureDetector(
                      onTap: () async {
                        await updateResult(true);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (Context) => qrCheckinScan(
                              locateRef: locateRef,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        child: Text(
                          'อนุญาติให้ \n Check-In',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                            fontFamily: 'Kanit',
                          ),
                        ),
                        margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
