import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';

class NavBar extends StatelessWidget {
  const NavBar({Key key}) : super(key: key);

  LogOut() async {
    userEmail = null;
    await FirebaseAuth.instance.signOut().whenComplete(
          () => print('Logout success'),
        );
    await google.signOut();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        UserAccountsDrawerHeader(
          accountName: Text(userEmail.displayName.toString()),
          accountEmail: Text(userEmail.email.toString()),
          currentAccountPicture: CircleAvatar(
            child: ClipOval(
              child: Image.network(
                userEmail.photoUrl?.toString(),
                width: 90,
                height: 90,
                fit: BoxFit.cover,
              ),
            ),
          ),
          decoration: BoxDecoration(
            color: Colors.blue,
            image: DecorationImage(
              image: NetworkImage(
                'https://data.1freewallpapers.com/download/calm-body-of-water-surrounded-with-trees-and-mountains-during-daytime-4k-nature.jpg',
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
        ListTile(
          leading: Icon(Icons.home),
          title: Text(
            'Home',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => HomePage())),
        ),
        ListTile(
          leading: Icon(Icons.emoji_emotions),
          title: Text(
            'Profile',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => ProfileScr())),
        ),
        ListTile(
          leading: Icon(Icons.book),
          title: Text(
            'หน่วยงาน',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => Navigator.push(
              context, MaterialPageRoute(builder: (context) => LocationPage())),
        ),
        ListTile(
          leading: Icon(Icons.exit_to_app),
          title: Text(
            'Sign out',
            style: TextStyle(
              fontFamily: 'Kanit',
            ),
          ),
          onTap: () => showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                'Are you sure?',
                style: TextStyle(
                  fontFamily: 'Kanit',
                ),
              ),
              content: Text(
                'Do you want to sign out from the app?',
                style: TextStyle(
                  fontFamily: 'Kanit',
                ),
              ),
              actions: [
                GestureDetector(
                  child: Text(
                    'SIGN OUT',
                    style: TextStyle(
                      fontFamily: 'Kanit',
                    ),
                  ),
                  onTap: () async {
                    await LogOut();
                    Navigator.pushNamed(context, '/login');
                  },
                ),
                GestureDetector(
                  child: Text(
                    'CANCLE',
                    style: TextStyle(
                      fontFamily: 'Kanit',
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    ));
  }
}
