import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class ResultATK extends StatefulWidget {
  ResultATK({Key key}) : super(key: key);

  @override
  State<ResultATK> createState() => _ResultATKState();
}

class _ResultATKState extends State<ResultATK> {
  String image;
  bool check = true;

  CollectionReference user = FirebaseFirestore.instance.collection('user');

  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController username = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    setState(() {
      user
          .doc(userEmail.email?.toString())
          .collection('atk_result')
          .orderBy('date_time', descending: false)
          .get()
          .then((QuerySnapshot querySnapshot) {
        check = querySnapshot.docs.last.get('result');
        image = querySnapshot.docs.last.get('image');
        DocumentReference<Map<String, dynamic>> atk =
            querySnapshot.docs.last.get('atk_brand');
        print(querySnapshot.docs.last.id);
        atk.get().then((value) {
          atkBrand.text = value['atk_brand'];
        });
        DocumentReference<Map<String, dynamic>> name =
            querySnapshot.docs.last.get('name');
        name.get().then((value) {
          username.text = value['name'];
        });
        var format = new DateFormat('d-MM-y, hh:mm:ss a');
        dateTime.text =
            format.format(querySnapshot.docs.last.get('date_time').toDate());
        GeoPoint geo = querySnapshot.docs.last.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;

  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              width: width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                              width: double.infinity,
                              padding: EdgeInsets.all(10.0),
                              decoration: new BoxDecoration(
                                color: check
                                    ? LightColors.kGreenSmooth
                                    : LightColors.kRedSmooth,
                              ),
                              child: new Center(
                                child: Text(
                                  check ? 'ผ่าน' : 'ไม่ผ่าน',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Kanit',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ))
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? Image.network(
                                  image,
                                  width: 150,
                                  fit: BoxFit.cover,
                                )
                              : Container(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  MyTextField(
                    label: 'ชื่อ',
                    enable: false,
                    controller: username,
                  ),
                  MyTextField(
                    label: 'เวลา',
                    controller: dateTime,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    controller: address,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'ATK',
                    controller: atkBrand,
                    enable: false,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      await Navigator.pushNamed(context, '/qrCode');
                    },
                    child: Container(
                      child: Text(
                        'รับรองผลการตรวจด้วย QR Code',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                      width: width - 40,
                      decoration: BoxDecoration(
                        color: LightColors.kBlue,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
