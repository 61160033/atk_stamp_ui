import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/theme/colors/light_colors.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class VerifileScreen extends StatefulWidget {
  final String result;
  VerifileScreen({Key key, this.result}) : super(key: key);
  @override
  _VerifileScreenState createState() => _VerifileScreenState(this.result);
}

class _VerifileScreenState extends State<VerifileScreen> {
  final String result;
  _VerifileScreenState(this.result);
  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController username = TextEditingController();
  String image;
  bool check = true;
  String updateId = '';

  @override
  void initState() {
    super.initState();
    setState(() {
      updateId = '';
    });
    getDate();
  }

  getDate() async {
    await setState(() {
      FirebaseFirestore.instance.doc(result).get().then((value) {
        image = value.get('image');
        DocumentReference<Map<String, dynamic>> atk = value.get('atk_brand');
        atk.get().then((brand) {
          atkBrand.text = brand.get('atk_brand');
        });
        DocumentReference<Map<String, dynamic>> name = value.get('name');
        name.get().then((name) {
          username.text = name.get('name');
        });
        var format = new DateFormat('d-MM-y, hh:mm:ss a');
        dateTime.text = format.format(value.get('date_time').toDate());
        GeoPoint geo = value.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
        check = value.get('result');
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;
  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  updateResult(bool verifi) async {
    await FirebaseFirestore.instance
        .doc(result)
        .update({
          'certified': verifi ? true : false,
          'certifier': FirebaseFirestore.instance
              .collection('user')
              .doc(userEmail.email.toString()),
          'confirm_dateTime': FieldValue.serverTimestamp()
        })
        .then((value) => print('User Updated'))
        .catchError((error) => print('Failed to update user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 40),
              width: width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                              width: double.infinity,
                              padding: EdgeInsets.all(10.0),
                              decoration: new BoxDecoration(
                                color: check
                                    ? LightColors.kGreenSmooth
                                    : LightColors.kRedSmooth,
                              ),
                              child: new Center(
                                child: new Text(
                                  check ? 'ผ่าน' : 'ไม่ผ่าน',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Kanit',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ))
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? Image.network(image, width: 200)
                              : Container(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  MyTextField(
                    label: 'ชื่อผู้ตรวจ',
                    enable: false,
                    controller: username,
                  ),
                  MyTextField(
                    label: 'เวลาที่ตรวจ',
                    enable: false,
                    controller: dateTime,
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    enable: false,
                    controller: address,
                  ),
                  MyTextField(
                    label: 'ยี่ห้อ ATK',
                    enable: false,
                    controller: atkBrand,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      updateResult(false);
                      Navigator.pushNamed(context, '/');
                    },
                    child: Container(
                      child: Text(
                        'ไม่อนุมัติ',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                      width: 130,
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      updateResult(true);
                      Navigator.pushNamed(context, '/');
                    },
                    child: Container(
                      child: Text(
                        'อนุมัติ',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                      width: 130,
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
