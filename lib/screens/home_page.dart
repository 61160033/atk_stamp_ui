import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:percent_indicator/percent_indicator.dart';

class HomePage extends StatefulWidget {
  static CircleAvatar calendarIcon() {
    return CircleAvatar(
      radius: 25.0,
      backgroundColor: LightColors.kGreen,
      child: Icon(
        Icons.open_in_new_rounded,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    _getResult();
    _getVerify();
  }

  var resultSize;
  var verifySize;

  _getResult() async {
    await FirebaseFirestore.instance
        .collection('user')
        .doc(await userEmail?.email.toString())
        .collection('atk_result')
        .where('certified', isEqualTo: false)
        .get()
        .then((value) {
      setState(() {
        resultSize = value.size;
      });
    });
  }

  _getVerify() async {
    await FirebaseFirestore.instance
        .collection('user')
        .doc(await userEmail?.email.toString())
        .collection('atk_result')
        .where('certified', isEqualTo: true)
        .get()
        .then((value) {
      setState(() {
        verifySize = value.size;
      });
    });
  }

  final Stream<QuerySnapshot> _resultStream = FirebaseFirestore.instance
      .collection('user')
      .doc(userEmail?.email.toString())
      .collection('check_in')
      .orderBy('date_time', descending: true)
      .snapshots();

  Text subheading(String title) {
    return Text(
      title,
      style: TextStyle(
        color: LightColors.kDarkBlue,
        fontSize: 20.0,
        fontWeight: FontWeight.w700,
        letterSpacing: 1.2,
        fontFamily: 'Kanit',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    final list = <Widget>[
      Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('ผลการตรวจ',
              style: TextStyle(
                fontFamily: 'Kanit',
                fontSize: 20,
                fontWeight: FontWeight.bold,
              )),
          Container(
            margin: EdgeInsets.fromLTRB(2, 5, 0, 2),
            width: 120,
            decoration: BoxDecoration(
              color: LightColors.kBlueBlossom,
              borderRadius: BorderRadius.circular(20),
            ),
            child: TextButton(
              style: ButtonStyle(),
              onPressed: () async {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CameraScreen()));
              },
              child: Text(
                'เพิ่มผลตรวจ',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 16,
                  fontFamily: 'Kanit',
                ),
              ),
            ),
          ),
        ],
      ),
      SizedBox(height: 15.0),
      GestureDetector(
        child: TaskColumn(
          icon: Icons.blur_circular,
          iconBackgroundColor: LightColors.kDarkYellow,
          title: 'ผลตรวจที่รอรับการรับรอง',
          subtitle: '$resultSize ผลตรวจ',
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AtkResultUnverified()));
        },
      ),
      SizedBox(height: 15.0),
      GestureDetector(
        child: TaskColumn(
          icon: Icons.check_circle_outline,
          iconBackgroundColor: LightColors.kBlue,
          title: 'ผลตรวจที่ได้รับการรับรองเรียบร้อย',
          subtitle: '$verifySize ผลตรวจ',
        ),
        onTap: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => AtkResultVerified()));
        },
      ),
    ];
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      drawer: NavBar(),
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Home",
          style: TextStyle(fontFamily: 'Kanit'),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/scanQr');
              },
              child: Icon(Icons.qr_code_scanner_rounded,
                  color: LightColors.kDarkBlue, size: 25.0),
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            TopContainer(
              height: 135,
              width: width,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 0.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          CircularPercentIndicator(
                            radius: 90.0,
                            lineWidth: 5.0,
                            animation: true,
                            percent: 0.75,
                            circularStrokeCap: CircularStrokeCap.round,
                            progressColor: LightColors.kRed,
                            backgroundColor: LightColors.kDarkYellow,
                            center: CircleAvatar(
                              backgroundColor: LightColors.kBlue,
                              radius: 35.0,
                              child: userEmail != null
                                  ? ClipOval(
                                      child: Image.network(
                                          userEmail?.photoUrl.toString()),
                                    )
                                  : Container(),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  userEmail?.displayName.toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontFamily: 'Kanit',
                                    color: LightColors.kDarkBlue,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                                Text(
                                  'Developer',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 26.0,
                                    fontFamily: 'Kanit',
                                    color: Colors.black45,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ]),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.transparent,
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: Column(
                        children: list,
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: ListView(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: <Widget>[
                          StreamBuilder(
                            stream: _resultStream,
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (snapshot.hasError) {
                                return Text('Something went wrong');
                              }
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return Text('Loading..');
                              }
                              return ListBody(
                                children: snapshot.data.docs.map((document) {
                                  DocumentReference locateRef =
                                      document.get('location');
                                  DocumentReference certifierRef =
                                      document.get('certifier');
                                  return FutureBuilder(
                                    future: Future.wait(
                                        [locateRef.get(), certifierRef.get()]),
                                    builder: (BuildContext context,
                                        AsyncSnapshot snapshot) {
                                      if (snapshot.hasData) {
                                        return GestureDetector(
                                          child: TaskContainer(
                                            title: 'เช็คอินที่ ' +
                                                snapshot.data[0]['location'],
                                            subtitle: 'ผู้รับรอง : ' +
                                                snapshot.data[1]['name'],
                                            boxColor: LightColors.kLavender,
                                          ),
                                          onTap: () async {
                                            print(document.id);
                                          },
                                        );
                                      } else {
                                        return Container();
                                      }
                                    },
                                  );
                                }).toList(),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Business',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            label: 'School',
          ),
        ],
        selectedItemColor: Colors.amber[800],
      ),
    );
  }
}
