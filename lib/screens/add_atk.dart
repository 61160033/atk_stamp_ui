import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:tflite/tflite.dart';
import 'package:firebase_storage/firebase_storage.dart';

class AddATK extends StatefulWidget {
  final File imgPath;
  final List output;

  const AddATK({Key key, @required this.imgPath, @required this.output})
      : super(key: key);
  @override
  State<AddATK> createState() => _AddATKState(this.imgPath, this.output);
}

class _AddATKState extends State<AddATK> {
  TextEditingController address = TextEditingController();

//------------- Tflite -------------
  File _image; //Image get from camera
  List _output; //Result from Tflite
  final picker = ImagePicker();
  _AddATKState(this._image, this._output);

  //--------- get locations -------------
  Geolocator geolocator = Geolocator();
  Placemark place;
  LatLng _currentLocation;

  _getLocation() async {
    try {
      var geolocator = await _determinePosition();
      setState(() {
        _currentLocation = LatLng(geolocator.latitude, geolocator.longitude);
      });
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  Future<String> uploadImage(File image) async {
    String fileName = basename(image.path);
    Reference storageRef =
        FirebaseStorage.instance.ref().child('upload/$fileName');
    await storageRef.putFile(image);
    return await storageRef.getDownloadURL();
  }

  Future<void> addAtk() async {
    String imgUrl = await uploadImage(_image);
    return await FirebaseFirestore.instance
        .collection('user')
        .doc(userEmail?.email.toString())
        .collection('atk_result')
        .add({
          'address':
              GeoPoint(_currentLocation.latitude, _currentLocation.longitude),
          'atk_brand':
              FirebaseFirestore.instance.collection('atk_list').doc(atkId),
          'certified': false,
          'checkin_list': FieldValue.arrayUnion([]),
          'date_time': FieldValue.serverTimestamp(),
          'image': imgUrl,
          'name': FirebaseFirestore.instance
              .collection('user')
              .doc(userEmail.email.toString()),
          'result': _output[0]['label'].substring(2).contains('Negative')
              ? true
              : false
        })
        .then((value) => print('User ATK result'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  @override
  void dispose() {
    Tflite?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              width: width,
              child: Column(
                children: [
                  Row(
                    children: [
                      Hero(
                        tag: 'backButton',
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CameraScreen()));
                          },
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: Icon(
                              Icons.arrow_back_ios,
                              size: 25,
                              color: LightColors.kDarkBlue,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                      Container(
                        child: Text(
                          'เพิ่มผลการตรวจ',
                          style: TextStyle(
                            fontSize: 30.0,
                            fontWeight: FontWeight.w700,
                            fontFamily: 'Kanit',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Text(
                              'รายละเอียด',
                              style: TextStyle(
                                fontSize: 24.0,
                                fontWeight: FontWeight.w700,
                                fontFamily: 'Kanit',
                              ),
                            ),
                            SizedBox(height: 10),
                            Center(
                              child: _output != null
                                  ? Container(
                                      width: 350,
                                      height: 55,
                                      decoration: new BoxDecoration(
                                        color: _output[0]['label']
                                                .toString()
                                                .substring(2)
                                                .contains('Negative')
                                            ? Colors.green
                                            : Colors.red,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: new Center(
                                        child: Text(
                                          _output[0]['label']
                                                  .toString()
                                                  .substring(2)
                                                  .contains('Negative')
                                              ? 'ผ่าน'
                                              : 'ไม่ผ่าน',
                                          style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
                                            fontFamily: 'Kanit',
                                          ),
                                        ),
                                      ),
                                    )
                                  : Container(
                                      width: 300,
                                      height: 55,
                                      padding: EdgeInsets.all(10.0),
                                      margin: EdgeInsets.only(right: 20),
                                      decoration: new BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      child: Center(
                                        child: Text(
                                          "กรุณาถ่ายรูปภาพ ATK",
                                          style: TextStyle(
                                            fontSize: 18,
                                            color: Colors.white,
                                            fontFamily: 'Kanit',
                                          ),
                                        ),
                                      )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        alignment: Alignment.topLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                padding: const EdgeInsets.all(10),
                                child: Column(children: <Widget>[
                                  Container(
                                      padding: const EdgeInsets.all(20),
                                      width: double.infinity,
                                      height: 350,
                                      child: Image.file(
                                        _image,
                                        width: 300,
                                        fit: BoxFit.cover,
                                      )),
                                ]),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
                  SizedBox(height: 20),
                  Container(
                    child: DropdownAtkList(),
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    controller: address,
                    enable: false,
                  ),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      await addAtk();
                      Navigator.pushNamed(context, '/result');
                    },
                    child: Container(
                      child: Text(
                        'ยืนยันผลการตรวจ',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                      width: width - 40,
                      decoration: BoxDecoration(
                        color: LightColors.kBlue,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
