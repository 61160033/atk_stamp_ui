import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class CurrentATKResultVertified extends StatefulWidget {
  final id;
  CurrentATKResultVertified({Key key, this.id}) : super(key: key);

  @override
  State<CurrentATKResultVertified> createState() =>
      _CurrentATKResultVertifiedState(this.id);
}

class _CurrentATKResultVertifiedState extends State<CurrentATKResultVertified> {
  final id;
  String image;
  bool check = true;
  List checkInList = <DocumentReference>[];
  var format = new DateFormat('d-MM-y, hh:mm:ss a');
  _CurrentATKResultVertifiedState(this.id);

  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController userName = TextEditingController();
  TextEditingController certifierName = TextEditingController();
  TextEditingController certifierDate = TextEditingController();

  @override
  void initState() {
    checkInList.clear();
    super.initState();
    _getResult();
  }

  _getResult() async {
    await setState(() {
      FirebaseFirestore.instance
          .collection('user')
          .doc(userEmail?.email)
          .collection('atk_result')
          .doc(id)
          .get()
          .then((value) {
        image = value.get('image');
        DocumentReference<Map<String, dynamic>> atk = value.get('atk_brand');
        atk.get().then((brand) {
          atkBrand.text = brand.get('atk_brand');
        });
        DocumentReference<Map<String, dynamic>> name = value.get('name');
        name.get().then((name) {
          userName.text = name.get('name');
        });
        DocumentReference<Map<String, dynamic>> certifier =
            value.get('certifier');
        certifier.get().then((value) {
          certifierName.text = value.get('name');
        });

        dateTime.text = format.format(value.get('date_time').toDate());
        certifierDate.text =
            format.format(value.get('confirm_dateTime').toDate());
        GeoPoint geo = value.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
        check = value.get('result');
        checkInList = value.get('checkin_list');
        for (var item in checkInList) {
          print(item.path);
        }
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;

  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              width: width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                              width: double.infinity,
                              padding: EdgeInsets.all(10.0),
                              decoration: new BoxDecoration(
                                color: check
                                    ? LightColors.kGreenSmooth
                                    : LightColors.kRedSmooth,
                              ),
                              child: new Center(
                                child: Text(
                                  check ? 'ผ่าน' : 'ไม่ผ่าน',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Kanit',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: MyTextField(
                              label: 'ชื่อผู้ยืนยัน',
                              enable: false,
                              controller: certifierName,
                            ),
                          ),
                          Expanded(
                            child: MyTextField(
                              label: 'เวลาที่ยืนยัน',
                              enable: false,
                              controller: certifierDate,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ))
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? Image.network(image, width: 200)
                              : Container(),
                        ),
                      ],
                    ),
                  ),
                  MyTextField(
                    label: 'ชื่อ',
                    enable: false,
                    controller: userName,
                  ),
                  MyTextField(
                    label: 'เวลา',
                    controller: dateTime,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    controller: address,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'ATK',
                    controller: atkBrand,
                    enable: false,
                  ),
                  Container(
                    color: Colors.transparent,
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    child: Column(
                      children: [
                        Container(
                          child: Text('ประวัติการเข้าสถานที่'),
                        ),
                        Container(
                          padding: EdgeInsets.all(4),
                          child: ListView(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: checkInList.isEmpty
                                ? [
                                    Container(
                                        child: Text('result not check in')),
                                  ]
                                : checkInList.map((result) {
                                    DocumentReference checkInRef = result;
                                    return FutureBuilder(
                                      future: Future.wait([checkInRef.get()]),
                                      builder: (BuildContext context,
                                          AsyncSnapshot checkRef) {
                                        DocumentReference locateRef =
                                            checkRef.data[0]['location'];
                                        return Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                child: FutureBuilder(
                                                  future: Future.wait(
                                                      [locateRef.get()]),
                                                  builder: (BuildContext
                                                          context,
                                                      AsyncSnapshot snapshot) {
                                                    return Container(
                                                      child: Text(
                                                        snapshot.data[0]
                                                            ['location'],
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                              Text(
                                                format.format(
                                                  checkRef.data[0]['date_time']
                                                      .toDate(),
                                                ),
                                              ),
                                              SizedBox(width: 10),
                                              Text(
                                                checkRef.data[0]
                                                        ['check_in_result']
                                                    ? 'อนุญาติให้เข้า'
                                                    : 'ไม่อนุญาติให้เข้า',
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    );
                                  }).toList(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (Context) => CurrentQrCreate(id: id),
                        ),
                      );
                    },
                    child: Container(
                      child: Text(
                        'รับรองผลการตรวจด้วย QR Code',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                      width: width - 40,
                      decoration: BoxDecoration(
                        color: LightColors.kBlue,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
