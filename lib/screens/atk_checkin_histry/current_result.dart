import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:flutter_task_planner_app/widgets/widgets.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

class CurrentATKReult extends StatefulWidget {
  final id;
  CurrentATKReult({Key key, this.id}) : super(key: key);

  @override
  State<CurrentATKReult> createState() => _ATKHistoryState(this.id);
}

class _ATKHistoryState extends State<CurrentATKReult> {
  final id;
  String image;
  bool check = true;
  _ATKHistoryState(this.id);

  CollectionReference user = FirebaseFirestore.instance.collection('user');

  TextEditingController atkBrand = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController dateTime = TextEditingController();
  TextEditingController userName = TextEditingController();

  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    await setState(() {
      user
          .doc(userEmail.email?.toString())
          .collection('atk_result')
          .doc(id)
          .get()
          .then((value) {
        image = value.get('image');
        DocumentReference<Map<String, dynamic>> atk = value.get('atk_brand');
        atk.get().then((brand) {
          atkBrand.text = brand.get('atk_brand');
        });
        DocumentReference<Map<String, dynamic>> name = value.get('name');
        name.get().then((name) {
          userName.text = name.get('name');
        });
        var format = new DateFormat('d-MM-y, hh:mm:ss a');
        dateTime.text = format.format(value.get('date_time').toDate());
        GeoPoint geo = value.get('address');
        setState(() {
          _currentLocation = LatLng(geo.latitude, geo.longitude);
        });
        _getLocation();
        check = value.get('result');
      });
    });
  }

  Placemark place;
  LatLng _currentLocation;

  _getLocation() async {
    try {
      List<Placemark> placemarks = await placemarkFromCoordinates(
          _currentLocation.latitude, _currentLocation.longitude);
      setState(() {
        place = placemarks[0];
        address.text = place.street +
            ', ' +
            place.locality +
            ', ' +
            place.subAdministrativeArea +
            ', ' +
            place.administrativeArea +
            ', ' +
            place.postalCode +
            ', ' +
            place.country;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              width: width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  ),
                  Container(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: new Container(
                              width: double.infinity,
                              padding: EdgeInsets.all(10.0),
                              decoration: new BoxDecoration(
                                color: check
                                    ? LightColors.kGreenSmooth
                                    : LightColors.kRedSmooth,
                              ),
                              child: new Center(
                                child: Text(
                                  check ? 'ผ่าน' : 'ไม่ผ่าน',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontFamily: 'Kanit',
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ))
                ],
              ),
            ),
            Expanded(
                child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: image != null
                              ? Image.network(image, width: 200)
                              : Container(),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  MyTextField(
                    label: 'ชื่อ',
                    enable: false,
                    controller: userName,
                  ),
                  MyTextField(
                    label: 'เวลา',
                    controller: dateTime,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'สถานที่ตรวจ',
                    controller: address,
                    enable: false,
                  ),
                  MyTextField(
                    label: 'ATK',
                    controller: atkBrand,
                    enable: false,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            )),
            Container(
              height: 80,
              width: width,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (Context) => CurrentQrCreate(id: id),
                        ),
                      );
                    },
                    child: Container(
                      child: Text(
                        'รับรองผลการตรวจด้วย QR Code',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      alignment: Alignment.center,
                      margin: EdgeInsets.fromLTRB(20, 10, 10, 20),
                      width: width - 40,
                      decoration: BoxDecoration(
                        color: LightColors.kBlue,
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
