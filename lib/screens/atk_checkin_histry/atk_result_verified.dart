import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:intl/intl.dart';

class AtkResultVerified extends StatefulWidget {
  @override
  State<AtkResultVerified> createState() => _AtkResultVerifiedState();
}

class _AtkResultVerifiedState extends State<AtkResultVerified> {
  @override
  void initState() {
    super.initState();
  }

  final Stream<QuerySnapshot> listVerified = FirebaseFirestore.instance
      .collection('user')
      .doc(userEmail?.email.toString())
      .collection('atk_result')
      .where('certified', isEqualTo: true)
      .snapshots();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: LightColors.kLightYellow,
      appBar: AppBar(
        backgroundColor: LightColors.kBlue,
        title: Text(
          "Verified ATK result",
          style: TextStyle(
            fontFamily: 'Kanit',
          ),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        physics: ScrollPhysics(),
        children: [
          StreamBuilder(
            stream: listVerified,
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasError) {
                return Text('Something went wrong');
              }
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Text('Loading..');
              }
              return ListBody(
                children: snapshot.data.docs.map((document) {
                  var format = new DateFormat('d-MM-y, hh:mm:ss a');
                  return FutureBuilder(
                      future: Future.wait([]),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            decoration: BoxDecoration(
                              color: Colors.cyan.shade100.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: EdgeInsets.all(5),
                            child: GestureDetector(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.network(
                                    document.get('image'),
                                    fit: BoxFit.cover,
                                    width: 100,
                                    height: 75,
                                  ),
                                  Expanded(
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 5),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              'วันที่ทดสอบ : ' +
                                                  format.format(document
                                                      .get('date_time')
                                                      .toDate()),
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontFamily: 'Kanit',
                                              )),
                                          Text(
                                              'ผลการยืนยัน : ' +
                                                  (document.get('certified')
                                                      ? 'ผ่าน'
                                                      : 'ไม่ผ่าน'),
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontFamily: 'Kanit',
                                              )),
                                          Text(
                                              'การยืนยัน : ' +
                                                  (document.get('certified')
                                                      ? 'ได้รับการยืนยันแล้ว'
                                                      : 'ยังไม่ได้รับการยืนยัน'),
                                              style: TextStyle(
                                                fontSize: 17,
                                                fontFamily: 'Kanit',
                                              )),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              onTap: () async {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (Context) =>
                                        CurrentATKResultVertified(
                                            id: document.id),
                                  ),
                                );
                                print(document.id);
                              },
                            ),
                          );
                        } else {
                          return Container();
                        }
                      });
                }).toList(),
              );
            },
          ),
        ],
      ),
    );
  }
}
