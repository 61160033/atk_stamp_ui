import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'package:qr_flutter/qr_flutter.dart';

class CurrentQrCreate extends StatefulWidget {
  final id;
  CurrentQrCreate({Key key, @required this.id}) : super(key: key);
  @override
  _CreateQRState createState() => _CreateQRState(this.id);
}

class _CreateQRState extends State<CurrentQrCreate> {
  final id;
  _CreateQRState(this.id);

  DocumentReference<Map<String, dynamic>> atk;
  @override
  void initState() {
    super.initState();
    _getResult();
  }

  _getResult() async {
    await FirebaseFirestore.instance
        .collection('user')
        .doc(userEmail?.email.toString())
        .collection('atk_result')
        .doc(id)
        .get()
        .then((value) {
      setState(() {
        atk = value.reference;
      });
      print('----------');
      print(atk?.path);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      body: ListView(
        children: [
          Center(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 50),
                  const Text(
                    'Create CODE',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'Kanit',
                      fontSize: 35,
                    ),
                  ),
                  const SizedBox(height: 20),
                  atk != null
                      ? QrImage(
                          data: atk?.path,
                          size: 350,
                          backgroundColor: Colors.white,
                        )
                      : Container(),
                  const SizedBox(height: 80),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/');
            },
            child: Container(
              height: 50,
              child: Text(
                'เสร็จสิ้น',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Kanit',
                    fontSize: 20),
              ),
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: LightColors.kBlue,
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          )
        ],
      ),
    );
  }
}
