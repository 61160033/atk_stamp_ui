import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task_planner_app/data/data.dart';
import 'package:flutter_task_planner_app/screens/login.dart';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:flutter/services.dart';
import 'package:flutter_task_planner_app/theme/theme.dart';
import 'firebase_options.dart';

List<CameraDescription> cameras = [];
Future<void> main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    cameras = await availableCameras();
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor:
          LightColors.kLightYellow, // navigation bar color
      statusBarColor: Color(0xffffb969), // status bar color
    ));
    return runApp(MyApp());
  } catch (e) {
    print(e);
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: Theme.of(context).textTheme.apply(
            bodyColor: LightColors.kDarkBlue,
            displayColor: LightColors.kDarkBlue,
            fontFamily: 'Poppins'),
      ),
      initialRoute: userEmail == null ? '/login' : '/',
      routes: {
        '/': (context) => HomePage(),
        '/login': (context) => Login(),
        '/scanQr': (context) => ScanQR(),
        '/result': (context) => ResultATK(),
        '/qrCode': (context) => CreateQR(),
        '/locations': (Context) => LocationPage(),
        '/profileQR': (context) => CreateQRProfile(),
        '/profileVr': (context) => ProfileVr(),
      },
    );
  }
}
