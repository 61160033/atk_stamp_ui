import 'dart:io';
import 'package:flutter_task_planner_app/screens/screens.dart';
import 'package:image/image.dart' as Image;
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tflite/tflite.dart';

import '../../main.dart';

class Bound {
  int x, y;
  int width, height;
  int maxWidth, maxHeight;
  Bound(
      {@required this.x,
      @required this.y,
      @required this.width,
      @required this.height,
      @required this.maxWidth,
      @required this.maxHeight});
  @override
  String toString() {
    return '$x $y $width $height';
  }
}

Bound cropBound =
    Bound(x: 0, y: 0, width: 0, height: 0, maxWidth: 0, maxHeight: 0);
Widget cameraOverlay(
    {@required double paddingPercent,
    @required double aspectRatio,
    @required Color color}) {
  return LayoutBuilder(builder: (context, constraints) {
    double parentAspectRatio = constraints.maxWidth / constraints.maxHeight;
    double horizontalPadding;
    double verticalPadding;
    double padding = constraints.maxWidth * paddingPercent / 200.0;

    if (parentAspectRatio < aspectRatio) {
      horizontalPadding = padding;
      verticalPadding = (constraints.maxHeight -
              ((constraints.maxWidth - 2 * padding) / aspectRatio)) /
          2;
    } else {
      verticalPadding = padding;
      horizontalPadding = (constraints.maxWidth -
              ((constraints.maxHeight - 2 * padding) * aspectRatio)) /
          2;
    }
    cropBound.maxWidth = constraints.maxWidth.round();
    cropBound.maxHeight = constraints.maxHeight.round();
    cropBound.x = horizontalPadding.round();
    cropBound.y = verticalPadding.round();
    cropBound.width = (constraints.maxWidth - 2 * horizontalPadding).round();
    cropBound.height = (constraints.maxHeight - 2 * verticalPadding).round();

    return Stack(fit: StackFit.expand, children: [
      Align(
          alignment: Alignment.centerLeft,
          child: Container(width: horizontalPadding, color: color)),
      Align(
          alignment: Alignment.centerRight,
          child: Container(width: horizontalPadding, color: color)),
      Align(
          alignment: Alignment.topCenter,
          child: Container(
              margin: EdgeInsets.only(
                  left: horizontalPadding, right: horizontalPadding),
              height: verticalPadding,
              color: color)),
      Align(
          alignment: Alignment.bottomCenter,
          child: Container(
              margin: EdgeInsets.only(
                  left: horizontalPadding, right: horizontalPadding),
              height: verticalPadding,
              color: color)),
      Container(
        margin: EdgeInsets.symmetric(
            horizontal: horizontalPadding, vertical: verticalPadding),
        decoration: BoxDecoration(border: Border.all(color: Colors.cyan)),
      )
    ]);
  });
}

class CameraScreen extends StatefulWidget {
  const CameraScreen({Key key}) : super(key: key);

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen>
    with WidgetsBindingObserver {
  CameraController controller;
  List _output; //Result from Tflite

  loadModel() async {
    await Tflite.loadModel(
      model: 'assets/models/model_unquant.tflite',
      labels: 'assets/models/labels.txt',
    );
  }

  classifyImage(File image) async {
    print(image.path);
    var output = await Tflite.runModelOnImage(
        path: image.path,
        numResults: 2,
        threshold: 0.2,
        imageMean: 0.0,
        imageStd: 255.0,
        asynch: true);

    setState(() {
      _output = output;
      print(_output);
    });
  }

  File _imageFile;

  // Initial values
  bool _isCameraInitialized = false;
  double _minAvailableExposureOffset = 0.0;
  double _maxAvailableExposureOffset = 0.0;
  double _minAvailableZoom = 1.0;
  double _maxAvailableZoom = 1.0;

  // Current values
  double _currentZoomLevel = 1.0;
  double _currentExposureOffset = 0.0;

  List<File> allFileList = [];

  final resolutionPresets = ResolutionPreset.values;

  ResolutionPreset currentResolutionPreset = ResolutionPreset.high;

  refreshAlreadyCapturedImages() async {
    final directory = await getApplicationDocumentsDirectory();
    List<FileSystemEntity> fileList = await directory.list().toList();
    allFileList.clear();
    List<Map<int, dynamic>> fileNames = [];

    for (var file in fileList) {
      if (file.path.contains('.jpg')) {
        allFileList.add(File(file.path));

        String name = file.path.split('/').last.split('.').first;
        fileNames.add({0: int.parse(name), 1: file.path.split('/').last});
      }
    }

    if (fileNames.isNotEmpty) {
      final recentFile =
          fileNames.reduce((curr, next) => curr[0] > next[0] ? curr : next);
      String recentFileName = recentFile[1];
      _imageFile = File('${directory.path}/$recentFileName');

      setState(() {});
    }
    print('-------log image refresh-------');
    print(_imageFile.path);
  }

  Future<XFile> takePicture() async {
    final CameraController cameraController = controller;

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      print('Error occured while taking picture: $e');
      return null;
    }
  }

  void resetCameraValues() async {
    _currentZoomLevel = 1.0;
    _currentExposureOffset = 0.0;
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    final previousCameraController = controller;

    final CameraController cameraController = CameraController(
      cameraDescription,
      currentResolutionPreset,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    await previousCameraController?.dispose();

    resetCameraValues();

    if (mounted) {
      setState(() {
        controller = cameraController;
      });
    }

    // Update UI if controller updated
    cameraController.addListener(() {
      if (mounted) setState(() {});
    });

    try {
      await cameraController.initialize();
      await Future.wait([
        cameraController
            .getMinExposureOffset()
            .then((value) => _minAvailableExposureOffset = value),
        cameraController
            .getMaxExposureOffset()
            .then((value) => _maxAvailableExposureOffset = value),
        cameraController
            .getMaxZoomLevel()
            .then((value) => _maxAvailableZoom = value),
        cameraController
            .getMinZoomLevel()
            .then((value) => _minAvailableZoom = value),
      ]);

      //_currentFlashMode = controller!.value.flashMode;
    } on CameraException catch (e) {
      print('Error initializing camera: $e');
    }

    if (mounted) {
      setState(() {
        _isCameraInitialized = controller.value.isInitialized;
      });
    }
  }

  @override
  void initState() {
    // Hide the status bar in Android
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    // Set and initialize the new camera
    onNewCameraSelected(cameras[0]);
    refreshAlreadyCapturedImages();
    super.initState();
    loadModel().then((value) {
      setState(() {});
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    Tflite?.close();
    _output?.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: _isCameraInitialized
            ? Column(
                children: [
                  AspectRatio(
                    aspectRatio: 1 / controller.value.aspectRatio,
                    child: Stack(
                      children: [
                        controller.buildPreview(),
                        cameraOverlay(
                            paddingPercent: 70.0,
                            aspectRatio: 1,
                            color: const Color(0x55000000)),
                        GestureDetector(
                          child: Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                            padding: EdgeInsets.all(8),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white.withOpacity(0.8)),
                            child: Text(
                              'return',
                              style: const TextStyle(
                                color: Colors.black45,
                                fontSize: 20,
                                fontFamily: 'Kanit',
                              ),
                            ),
                          ),
                          onTap: () {
                            Navigator.pushNamed(context, '/');
                          },
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    right: 8.0, top: 16.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      _currentExposureOffset
                                              .toStringAsFixed(1) +
                                          'x',
                                      style:
                                          const TextStyle(color: Colors.black),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                child: RotatedBox(
                                  quarterTurns: 3,
                                  child: Container(
                                    height: 30,
                                    child: Slider(
                                      value: _currentExposureOffset,
                                      min: _minAvailableExposureOffset,
                                      max: _maxAvailableExposureOffset,
                                      activeColor: Colors.white,
                                      inactiveColor: Colors.white30,
                                      onChanged: (value) async {
                                        setState(() {
                                          _currentExposureOffset = value;
                                        });
                                        await controller
                                            .setExposureOffset(value);
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: Slider(
                                      value: _currentZoomLevel,
                                      min: _minAvailableZoom,
                                      max: _maxAvailableZoom,
                                      activeColor: Colors.white,
                                      inactiveColor: Colors.white30,
                                      onChanged: (value) async {
                                        setState(() {
                                          _currentZoomLevel = value;
                                        });
                                        await controller.setZoomLevel(value);
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 8.0),
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.black87,
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          _currentZoomLevel.toStringAsFixed(1) +
                                              'x',
                                          style: const TextStyle(
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () async {
                                      XFile rawImage = await takePicture();
                                      print('------raw image-------//');

                                      File imageFile = File(rawImage.path);
                                      print(rawImage.path.split('/').last);

                                      print('------image-------//');
                                      print(imageFile.path.split('/').last);
                                      int currentUnix =
                                          DateTime.now().millisecondsSinceEpoch;

                                      final directory =
                                          await getApplicationDocumentsDirectory();

                                      String fileFormat =
                                          imageFile.path.split('.').last;
                                      print('--------fileFormat---------//');
                                      print(fileFormat);

                                      File imgPath;
                                      imgPath = await imageFile.copy(
                                        '${directory.path}/$currentUnix.$fileFormat',
                                      );
                                      print('------New Image path------');
                                      print(
                                          '${directory.path}/$currentUnix.$fileFormat');
                                      final image = Image.decodeImage(
                                          imageFile.readAsBytesSync());
                                      double ratioWidth =
                                          image.width / cropBound.maxWidth;
                                      double ratioHeight =
                                          image.height / cropBound.maxHeight;
                                      print('--------------------------');
                                      print('$ratioWidth $ratioHeight');
                                      final cropImage = Image.copyCrop(
                                          image,
                                          (cropBound.x * ratioWidth).round(),
                                          (cropBound.y * ratioHeight).round(),
                                          (cropBound.width * ratioWidth)
                                              .round(),
                                          (cropBound.height * ratioHeight)
                                              .round());
                                      print('----------cropImage------------');
                                      File imgCrop;
                                      if (fileFormat == "jpg") {
                                        imgCrop = await File(
                                                '${directory.path}/${currentUnix}5.$fileFormat')
                                            .writeAsBytes(
                                                Image.encodeJpg(cropImage));
                                      } else if (fileFormat == "png") {
                                        imgCrop = await File(
                                                '${directory.path}/${currentUnix}5.$fileFormat')
                                            .writeAsBytes(
                                                Image.encodePng(cropImage));
                                      }
                                      print(imgCrop.path.split('/').last);
                                      print('//--------------------//');
                                      //print(imagePath.path);
                                      await classifyImage(imgCrop);
                                      await refreshAlreadyCapturedImages();
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => AddATK(
                                            imgPath: imgPath,
                                            output: _output,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Stack(
                                      alignment: Alignment.center,
                                      children: const [
                                        Icon(Icons.circle,
                                            color: Colors.white38, size: 80),
                                        Icon(Icons.circle,
                                            color: Colors.white, size: 65),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : const Center(
                child: Text(
                  'LOADING',
                  style: TextStyle(color: Colors.white),
                ),
              ),
      ),
    );
  }
}
