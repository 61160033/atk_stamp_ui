import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

String atkId = '';

class DropdownAtkList extends StatefulWidget {
  DropdownAtkList({Key key}) : super(key: key);
  @override
  State<DropdownAtkList> createState() => _DropdownAtkListState();
}

class _DropdownAtkListState extends State<DropdownAtkList> {
  String _dropdownValue;

  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance.collection('atk_list').get().then((value) {
      setState(() {
        value.docs.forEach((element) {
          atks.add(Atk(
            id: element.id.toString(),
            name: element['atk_brand'].toString(),
          ));
        });
      });
    });
  }

  var atks = <Atk>[];
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        isExpanded: true,
        hint: Text('กรุณาเลือกยี่ห้อ ATK'),
        value: _dropdownValue,
        icon: Icon(Icons.arrow_drop_down, size: 40, color: Colors.grey),
        elevation: 16,
        style: TextStyle(fontFamily: 'Kanit',color: Colors.black),
        onChanged: (String newValue) {
          setState(() {
            _dropdownValue = newValue;
            atkId = newValue;
          });
          print('select Atk: $atkId');
        },
        items: atks.map((Atk e) {
          return DropdownMenuItem(
            value: e.id,
            child: Text(e.name),
          );
        }).toList());
  }
}

class Atk {
  String id;
  String name;
  Atk({@required this.id, @required this.name});
  @override
  String toString() {
    return 'id: ${this.id}, name: ${this.name}';
  }
}
