import 'package:flutter/material.dart';

class ActiveProjectsCard extends StatelessWidget {
  final Color cardColor;
  final int timesCheckin;
  final String title;
  final String subtitle;
  final String picture;

  ActiveProjectsCard(
      {this.cardColor,
      this.timesCheckin,
      this.title,
      this.subtitle,
      this.picture});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        padding: EdgeInsets.all(15.0),
        height: 200,
        decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Text(
                '${(timesCheckin)} ครั้ง',
                style: TextStyle(
                    fontSize: 30,
                    fontFamily: 'Kanit',
                    fontWeight: FontWeight.w700,
                    color: Colors.white),
              ),
            ),
            Column(
              children: [Image.asset('${(picture)}', width: 100, height: 65)],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: 'Kanit',
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontFamily: 'Kanit',
                    color: Colors.white54,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
